#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Create new dataset for Triplet Network
"""

import numpy as np
import pandas as pd

#Set working directory.
import os
os.chdir("C:/Users/user/Desktop/internship")

train = pd.read_csv("train.csv")  # ./src/

train_context_inputs = train.Context
train_response_inputs = train.Utterance
train_targets = train.Label

ones = train['Label'] == 1
zeros = train['Label'] == 0

train_correct = train[ones]
train_wrong = train[zeros]

# Will use these answers as wrong ones.
possible_answers = list(train_wrong['Utterance'])

# Create the new dataframe.
new_data = train_correct[:25600]

# How many negative samples to put for each positive sample.
neg_samples = 10
new_data = pd.concat([new_data] * neg_samples, ignore_index=True)

np.random.seed(1234)
new_data['Wrong'] = pd.Series(np.random.choice(a=possible_answers, size=len(new_data),
                                               replace=True), index=new_data.index)

# Save the new dataset to csv.
new_data.to_csv('new_data_10.csv', sep='\t')
