from tqdm import tqdm

#from utils.word_representation import GloVe

import tensorflow as tf
import numpy as np
import pandas as pd
from nltk.stem.snowball import SnowballStemmer
import nltk
nltk.download('stopwords')


import os
os.chdir("C:/Users/user/Desktop/internship")

class RNN_Data_Loader:

    filename = 'glove.6B.300d.txt'

    def __init__(self, config, file_res):
        self.config = config

        self.train = pd.read_csv("C:/Users/user/Desktop/internship/train_.csv")[:999424] #./src/ 249856
        self.valid = pd.read_csv("C:/Users/user/Desktop/internship/valid.csv")[:18688] #./src/ 18688
        self.test = pd.read_csv("C:/Users/user/Desktop/internship/test.csv")[:18688] #./src/  18688

        print('import data')
        file_res.write('import data\n')

        print('train: ', self.train.shape)
        print('valid: ', self.valid.shape)
        print('test: ', self.test.shape)

        self.train_len = len(self.train)
        self.valid_len = len(self.valid)
        self.test_len = len(self.test)

        #self.num_iterations_train = (self.train_len + self.config.EPOCHS - 1) // self.config.EPOCHS
        #self.num_iterations_valid = (self.valid_len + self.config.EPOCHS - 1) // self.config.EPOCHS
        #self.num_iterations_test = (self.test_len + self.config.EPOCHS - 1) // self.config.EPOCHS

        # Prepare training and testing data.
        self.train_context_inputs = self.train.Context
        self.train_response_inputs = self.train.Utterance
        self.train_targets = self.train.Label

        self.valid_context_inputs = self.valid.Context
        self.valid_response_inputs = self.valid['Ground Truth Utterance']
        self.valid_Distractor_0_inputs = self.valid['Distractor_0']
        self.valid_Distractor_1_inputs = self.valid['Distractor_1']
        self.valid_Distractor_2_inputs = self.valid['Distractor_2']
        self.valid_Distractor_3_inputs = self.valid['Distractor_3']
        self.valid_Distractor_4_inputs = self.valid['Distractor_4']
        self.valid_Distractor_5_inputs = self.valid['Distractor_5']
        self.valid_Distractor_6_inputs = self.valid['Distractor_6']
        self.valid_Distractor_7_inputs = self.valid['Distractor_7']
        self.valid_Distractor_8_inputs = self.valid['Distractor_8']
        self.valid_targets = np.ones(len(self.valid))

        self.test_context_inputs = self.test.Context
        self.test_response_inputs = self.test['Ground Truth Utterance']
        self.test_Distractor_0_inputs = self.test['Distractor_0']
        self.test_Distractor_1_inputs = self.test['Distractor_1']
        self.test_Distractor_2_inputs = self.test['Distractor_2']
        self.test_Distractor_3_inputs = self.test['Distractor_3']
        self.test_Distractor_4_inputs = self.test['Distractor_4']
        self.test_Distractor_5_inputs = self.test['Distractor_5']
        self.test_Distractor_6_inputs = self.test['Distractor_6']
        self.test_Distractor_7_inputs = self.test['Distractor_7']
        self.test_Distractor_8_inputs = self.test['Distractor_8']
        self.test_targets = np.ones(len(self.test))




        self.vocab, self.embd = RNN_Data_Loader.loadGloVe(RNN_Data_Loader.filename)
        self.vocab_size = len(self.vocab)
        embedding_dim = len(self.embd[0])
        # embedding = np.array(embd)
        self.embedding = np.vstack(self.embd)

        # stemm
        # Stemm the vocab of GloVe.
        self.stemmer = SnowballStemmer("english", ignore_stopwords=True)
        self.vocab = [self.stemmer.stem(word) for word in self.vocab]
        # print(stemmer.stem("maybe"))

        # recreate
        # Recreate the vocab and embedding lists after stemming, since they reduce their size.
        self.new_data = pd.DataFrame({'vocab': self.vocab, 'emd': list(self.embedding[1:])})
        self.new_data = self.new_data.drop_duplicates('vocab')
        self.vocab = list(self.new_data.loc[:, 'vocab'])
        self.embedding = list(self.new_data.loc[:, 'emd'])
        self.embedding.insert(0, np.zeros(300))

        # Replacing some words that are popular so to match the ones in GloVe.
        words_to_change = ['onli', 'dpkg', 'xorg.conf', 'livecd', 'gpart', 'fstab']
        changed_with = ['only', 'softwar', 'config', 'cd', 'editor', 'file']

        print("Replacing some words that are popular so to match the ones in GloVe")

        for w in tqdm(range(len(words_to_change)), desc="Replacing some words"):
            self.train_context_inputs = self.train_context_inputs.str.replace(words_to_change[w], changed_with[w])
            self.train_response_inputs = self.train_response_inputs.str.replace(words_to_change[w], changed_with[w])

            self.valid_context_inputs = self.valid_context_inputs.str.replace(words_to_change[w], changed_with[w])
            self.valid_response_inputs = self.valid_response_inputs.str.replace(words_to_change[w], changed_with[w])
            self.valid_Distractor_0_inputs = self.valid_Distractor_0_inputs.str.replace(words_to_change[w], changed_with[w])
            self.valid_Distractor_1_inputs = self.valid_Distractor_1_inputs.str.replace(words_to_change[w], changed_with[w])
            self.valid_Distractor_2_inputs = self.valid_Distractor_2_inputs.str.replace(words_to_change[w], changed_with[w])
            self.valid_Distractor_3_inputs = self.valid_Distractor_3_inputs.str.replace(words_to_change[w], changed_with[w])
            self.valid_Distractor_4_inputs = self.valid_Distractor_4_inputs.str.replace(words_to_change[w], changed_with[w])
            self.valid_Distractor_5_inputs = self.valid_Distractor_5_inputs.str.replace(words_to_change[w], changed_with[w])
            self.valid_Distractor_6_inputs = self.valid_Distractor_6_inputs.str.replace(words_to_change[w], changed_with[w])
            self.valid_Distractor_7_inputs = self.valid_Distractor_7_inputs.str.replace(words_to_change[w], changed_with[w])
            self.valid_Distractor_8_inputs = self.valid_Distractor_8_inputs.str.replace(words_to_change[w], changed_with[w])

            self.test_context_inputs = self.test_context_inputs.str.replace(words_to_change[w], changed_with[w])
            self.test_response_inputs = self.test_response_inputs.str.replace(words_to_change[w], changed_with[w])
            self.test_Distractor_0_inputs = self.test_Distractor_0_inputs.str.replace(words_to_change[w], changed_with[w])
            self.test_Distractor_1_inputs = self.test_Distractor_1_inputs.str.replace(words_to_change[w], changed_with[w])
            self.test_Distractor_2_inputs = self.test_Distractor_2_inputs.str.replace(words_to_change[w], changed_with[w])
            self.test_Distractor_3_inputs = self.test_Distractor_3_inputs.str.replace(words_to_change[w], changed_with[w])
            self.test_Distractor_4_inputs = self.test_Distractor_4_inputs.str.replace(words_to_change[w], changed_with[w])
            self.test_Distractor_5_inputs = self.test_Distractor_5_inputs.str.replace(words_to_change[w], changed_with[w])
            self.test_Distractor_6_inputs = self.test_Distractor_6_inputs.str.replace(words_to_change[w], changed_with[w])
            self.test_Distractor_7_inputs = self.test_Distractor_7_inputs.str.replace(words_to_change[w], changed_with[w])
            self.test_Distractor_8_inputs = self.test_Distractor_8_inputs.str.replace(words_to_change[w], changed_with[w])





        # add_popular
        # Add some words not present in GloVe but very popular in the train data.
        # Will give them a random embedding from Uniform(-0.25,0.25).
        # The most common words in the train set are:
        # print(sorted_counts[:10])
        self.vocab.append('__eou__')
        self.embedding = np.vstack((self.embedding, np.random.uniform(-0.25, 0.25, size=300)))
        self.vocab.append('__eot__')
        self.embedding = np.vstack((self.embedding, np.random.uniform(-0.25, 0.25, size=300)))

        # processor
        self.vocab_processor = tf.contrib.learn.preprocessing.VocabularyProcessor(self.config.MAX_DOCUMENT_LENGTH,
                                                                                  tokenizer_fn=RNN_Data_Loader.tokenizer_fn)
        # Fit the vocab from GloVe.
        self.pretrain = self.vocab_processor.fit(self.vocab)

        # Create dictionary of words:id.
        self.vocab_dict = self.vocab_processor.vocabulary_._mapping

        # Create dictionary of id:words, the inverse of the previous one.
        self.vocab_dict_inv = {v: k for k, v in self.vocab_dict.items()}







    #transform_data
        # Transform training data.
        self.train_context_ids = np.array(list(self.vocab_processor.transform(self.train_context_inputs)))
        self.train_response_ids = np.array(list(self.vocab_processor.transform(self.train_response_inputs)))

        # Transform validation data.
        self.valid_context_ids = np.array(list(self.vocab_processor.transform(self.valid_context_inputs)))
        self.valid_response_ids = np.array(list(self.vocab_processor.transform(self.valid_response_inputs)))
        self.valid_Distractor_0_ids = np.array(list(self.vocab_processor.transform(self.valid_Distractor_0_inputs)))
        self.valid_Distractor_1_ids = np.array(list(self.vocab_processor.transform(self.valid_Distractor_1_inputs)))
        self.valid_Distractor_2_ids = np.array(list(self.vocab_processor.transform(self.valid_Distractor_2_inputs)))
        self.valid_Distractor_3_ids = np.array(list(self.vocab_processor.transform(self.valid_Distractor_3_inputs)))
        self.valid_Distractor_4_ids = np.array(list(self.vocab_processor.transform(self.valid_Distractor_4_inputs)))
        self.valid_Distractor_5_ids = np.array(list(self.vocab_processor.transform(self.valid_Distractor_5_inputs)))
        self.valid_Distractor_6_ids = np.array(list(self.vocab_processor.transform(self.valid_Distractor_6_inputs)))
        self.valid_Distractor_7_ids = np.array(list(self.vocab_processor.transform(self.valid_Distractor_7_inputs)))
        self.valid_Distractor_8_ids = np.array(list(self.vocab_processor.transform(self.valid_Distractor_8_inputs)))

        # Transform testing data.
        self.test_context_ids = np.array(list(self.vocab_processor.transform(self.test_context_inputs)))
        self.test_response_ids = np.array(list(self.vocab_processor.transform(self.test_response_inputs)))
        self.test_Distractor_0_ids = np.array(list(self.vocab_processor.transform(self.test_Distractor_0_inputs)))
        self.test_Distractor_1_ids = np.array(list(self.vocab_processor.transform(self.test_Distractor_1_inputs)))
        self.test_Distractor_2_ids = np.array(list(self.vocab_processor.transform(self.test_Distractor_2_inputs)))
        self.test_Distractor_3_ids = np.array(list(self.vocab_processor.transform(self.test_Distractor_3_inputs)))
        self.test_Distractor_4_ids = np.array(list(self.vocab_processor.transform(self.test_Distractor_4_inputs)))
        self.test_Distractor_5_ids = np.array(list(self.vocab_processor.transform(self.test_Distractor_5_inputs)))
        self.test_Distractor_6_ids = np.array(list(self.vocab_processor.transform(self.test_Distractor_6_inputs)))
        self.test_Distractor_7_ids = np.array(list(self.vocab_processor.transform(self.test_Distractor_7_inputs)))
        self.test_Distractor_8_ids = np.array(list(self.vocab_processor.transform(self.test_Distractor_8_inputs)))

        # Print total number of words.
        self.n_words = len(self.vocab_processor.vocabulary_)
        print('Total words: %d' % self.n_words)
        file_res.write('Total words: %d \n' % self.n_words)

    #def initialize(self, sess):
     #   sess.run(self.training_init_op)

    def get_input(self):
        context_placeholder = tf.placeholder(shape=[None, self.config.MAX_DOCUMENT_LENGTH], dtype=tf.int32)
        response_placeholder = tf.placeholder(shape=[None, self.config.MAX_DOCUMENT_LENGTH], dtype=tf.int32)
        y_target_placeholder = tf.placeholder(shape=[None, 1], dtype=tf.float32)
        embedding_placeholder = tf.placeholder(shape=[self.n_words, self.config.EMBEDDING_SIZE], dtype=tf.float32)
        learn_rate = tf.placeholder(tf.float32, shape=[])
        return context_placeholder, response_placeholder, y_target_placeholder, embedding_placeholder, learn_rate
     #   return self.iterator.get_next()

    @staticmethod
    def loadGloVe(filename):
        vocab = []
        # Had first row of embd all zeros, it will be the embedding for words
        # not found that will recieve id zero from the processor following.
        embd = [[np.zeros(300)]]
        file = open(filename, encoding="utf8")
        for line in file.readlines():
            row = line.strip().split(' ')
            vocab.append(row[0])
            embd.append([float(i) for i in row[1:]])
        print('Loaded GloVe!')
        file.close()
        return vocab, embd


    @staticmethod
    # Init vocab processor.
    def tokenizer_fn(iterator):
        return (x.split(" ")[-65:] for x in iterator)

