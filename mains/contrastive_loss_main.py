import sys

sys.path.extend(['..'])

import tensorflow as tf

from data_loader.contrastive_loss_data_generator import Contrastive_Loss_Data_Loader
from models.contrastive_loss_model import Contrastive_Loss_Model
from trainers.contrastive_loss_trainer import Contrastive_Loss_Trainer

from utils.config import process_config
from utils.dirs import create_dirs
from utils.logger import DefinedSummarizer
from utils.utils import get_args

import os
os.chdir("C:/Users/user/Desktop/internship")
#"C:/Users/user/Desktop/internship/configs/RNN_LSTM_Configuration.json"

def main():
    # capture the config path from the run arguments
    # then process the json configration file
    try:
        #args = get_args()
        #config = process_config(args.config)
        config = process_config("C:/Users/user/Desktop/internship/question_answering_system/configs/contrastive_loss_Configuration.json")
        print('json loaded')

    except:
        print("missing or invalid arguments")
        exit(0)

    # load data here
    file_res = open('C:/Users/user/Desktop/internship/results/contrastive_loss_results.txt', 'w')

    # create the experiments dirs
    create_dirs([config.summary_dir, config.checkpoint_dir])

    # create tensorflow session
    sess = tf.Session()

    # create your data generator
    data_loader = Contrastive_Loss_Data_Loader(config, file_res)


    # create instance of the model you want
    model = Contrastive_Loss_Model(data_loader, config)

    # create tensorboard logger
    logger = DefinedSummarizer(sess, config, summary_dir=config.summary_dir, scalar_tags=['train/loss_per_epoch', 'train/acc_per_epoch',
                                                                                  'valid/loss_per_epoch', 'valid/acc_per_epoch',
                                                                                  'test/loss_per_epoch','test/acc_per_epoch'])

    # create trainer and path all previous components to it
    trainer = Contrastive_Loss_Trainer(sess, model, config, logger, data_loader)

    # here you train your model
    trainer.train(file_res)


if __name__ == '__main__':
    main()
