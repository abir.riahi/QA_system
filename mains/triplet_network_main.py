import sys

sys.path.extend(['..'])

import tensorflow as tf

from data_loader.triplet_network_data_generator import Triplet_Network_Data_Loader
from models.triplet_network_model import Triplet_Network_Model
from trainers.triplet_network_trainer import Triplet_Network_Trainer

from utils.config import process_config
from utils.dirs import create_dirs
from utils.logger import DefinedSummarizer
from utils.utils import get_args

import os
os.chdir("C:/Users/user/Desktop/internship")
#"C:/Users/user/Desktop/internship/configs/RNN_LSTM_Configuration.json"

def main():
    # capture the config path from the run arguments
    # then process the json configration file
    try:
        #args = get_args()
        #config = process_config(args.config)
        config = process_config("C:/Users/user/Desktop/internship/question_answering_system/configs/triplet_network_Configuration.json")
        print('json loaded')

    except:
        print("missing or invalid arguments")
        exit(0)

    # load data here
    file_res = open('C:/Users/user/Desktop/internship/results/triplet_network_results.txt', 'w')

    # create the experiments dirs
    create_dirs([config.summary_dir, config.checkpoint_dir])

    # create tensorflow session
    sess = tf.Session()

    # create your data generator
    data_loader = Triplet_Network_Data_Loader(config, file_res)


    # create instance of the model you want
    model = Triplet_Network_Model(data_loader, config)

    # create tensorboard logger
    logger = DefinedSummarizer(sess, config, summary_dir=config.summary_dir, scalar_tags=['train/loss_per_epoch', 'train/acc_per_epoch',
                                                                                  'valid/loss_per_epoch', 'valid/acc_per_epoch',
                                                                                  'test/loss_per_epoch','test/acc_per_epoch'])

    # create trainer and path all previous components to it
    trainer = Triplet_Network_Trainer(sess, model, config, logger, data_loader)

    # here you train your model
    trainer.train(file_res)

if __name__ == '__main__':
    main()
