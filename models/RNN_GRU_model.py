from base.base_model import BaseModel

from data_loader.RNN_data_generator import RNN_Data_Loader

import tensorflow as tf
#import matplotlib.pyplot as plt
import numpy as np
from tensorflow.python.framework import ops
import pandas as pd
from tqdm import tqdm
#import operator
from nltk.stem.snowball import SnowballStemmer
import nltk
nltk.download('stopwords')
ops.reset_default_graph()


class RNN_GRU_Model(BaseModel):

    def __init__(self, data_loader, config):
        super(RNN_GRU_Model, self).__init__(config)
        # Get the data_loader to make the joint of the inputs in the graph
        self.data_loader = data_loader

        # define some important variables
        self.context_placeholder = []
        self.response_placeholder = []
        self.y_target_placeholder = []
        self.embedding_placeholder = []
        self.learn_rate = None
        #self.is_training = None
        self.model_output = None
        self.loss = None
        self.accuracy = None
        self.my_opt = None
        self.train_step = None

        self.build_model()
        self.init_saver()


    def build_model(self):
        # here you build the tensorflow graph of any model you want and also define the loss.

        """
        :return:
        """

        """
        Helper Variables
        """
        #self.global_step_tensor = tf.Variable(0, trainable=False, name='global_step')
        #self.global_step_inc = self.global_step_tensor.assign(self.global_step_tensor + 1)
        self.global_epoch_tensor = tf.Variable(0, trainable=False, name='global_epoch')
        self.global_epoch_inc = self.global_epoch_tensor.assign(self.global_epoch_tensor + 1)

        """
        Inputs to the network
        """
        with tf.variable_scope('inputs'):
            A = tf.Variable(tf.random_normal(shape=[self.config.NEURONS, self.config.NEURONS]))
            b = tf.Variable(tf.random_normal(shape=[1, 1]))
            # Initialize placeholders.
            self.context_placeholder, self.response_placeholder, self.y_target_placeholder, self.embedding_placeholder, self.learn_rate = self.data_loader.get_input()
            #self.is_training = tf.placeholder(tf.bool, name='Training_flag')
        tf.add_to_collection('inputs', self.context_placeholder)
        tf.add_to_collection('inputs', self.response_placeholder)
        tf.add_to_collection('inputs', self.y_target_placeholder)
        tf.add_to_collection('inputs', self.embedding_placeholder)
        tf.add_to_collection('inputs', self.learn_rate)
        #tf.add_to_collection('inputs', self.is_training)

        """
        Network Architecture
        """

        #TODO batch normalization ; dropout

        # RNN for the context.
        with tf.variable_scope('context'):
            word_vectors_contex = tf.nn.embedding_lookup(self.embedding_placeholder, self.context_placeholder)
            # RETURNS: Tensor of [batch_size, MAX_DOCUMENT_LENGTH, EMBEDDING_SIZE] with embedded sequences.

            word_list_context = tf.unstack(word_vectors_contex, axis=1)  # [batch_size, EMBEDDING_SIZE]
            cell1 = tf.nn.rnn_cell.GRUCell(self.config.NEURONS,
                                            activation=tf.sigmoid,
                                            initializer=tf.orthogonal_initializer())
            output1, final_state_context = tf.nn.static_rnn(cell1, word_list_context, dtype=tf.float32)
            #output1 = tf.layers.batch_normalization(output1, training=is_training)
            # activation must be done after batch normalization ?

            # RNN for the response.
        with tf.variable_scope('response'):
            word_vectors_contex_decoder = tf.nn.embedding_lookup(self.embedding_placeholder, self.response_placeholder)

            word_list_decoder = tf.unstack(word_vectors_contex_decoder, axis=1)
            cell2 = tf.nn.rnn_cell.GRUCell(self.config.NEURONS,
                                            activation=tf.sigmoid,
                                            initializer=tf.orthogonal_initializer())
            output2, final_state_response = tf.nn.static_rnn(cell2, word_list_decoder,
                                                             initial_state=final_state_context,
                                                             dtype=tf.float32)
            #output2 = tf.layers.batch_normalization(output2, training=is_training)

        with tf.variable_scope('out'):
            A = tf.tile(tf.expand_dims(A, axis=0), [self.config.batch_size, 1, 1])

            final_state_context = tf.reshape(final_state_context[1], [self.config.batch_size, self.config.NEURONS, 1])
            final_state_response = tf.reshape(final_state_response[1], [self.config.batch_size, self.config.NEURONS, 1])
            self.model_output = tf.add(tf.matmul(tf.matmul(final_state_context, A, transpose_a=True)
                                            , final_state_response), b)
            tf.add_to_collection('out', self.model_output)

            self.model_output = tf.reshape(self.model_output, [self.config.batch_size, 1])
            #self.model_output = tf.layers.batch_normalization(self.model_output, training=is_training)


        """
        Some operators for the training process
        """
        with tf.variable_scope('out_argmax'):
            self.prob_prediction = tf.sigmoid(self.model_output)

        with tf.variable_scope('loss-acc'):
            # Declare loss function (Cross Entropy loss).
            self.loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=self.model_output,
                                                                          labels=self.y_target_placeholder))
            # Prediction 0/1
            prediction = tf.round(tf.sigmoid(self.model_output))

            predictions_correct = tf.cast(tf.equal(prediction, self.y_target_placeholder), tf.float32)
            self.accuracy = tf.reduce_mean(predictions_correct)

        with tf.variable_scope('train_step'):
            #learn_rate = tf.placeholder(tf.float32, shape=[])
            # Declare optimizer.
            self.my_opt = tf.train.AdamOptimizer(learning_rate=self.learn_rate,
                                            beta1=0.9,
                                            beta2=0.999,
                                            epsilon=1e-08)  # 0.0025 GradientDescentOptimizer
            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies(update_ops):
                self.train_step = self.my_opt.minimize(self.loss)
                #self.train_step = self.my_opt.minimize(self.loss, global_step = self.global_step_tensor)

        tf.add_to_collection('train', self.train_step)
        tf.add_to_collection('train', self.loss)
        tf.add_to_collection('train', self.accuracy)

    def init_saver(self):
        """
        initialize the tensorflow saver that will be used in saving the checkpoints.
        :return:
        """
        #self.saver = tf.train.Saver(max_to_keep=self.config.max_to_keep, save_relative_paths=True)
        self.saver = tf.train.Saver()


