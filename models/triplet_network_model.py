#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Triplet Network
"""
from base.base_model import BaseModel

import tensorflow as tf
#import matplotlib.pyplot as plt
from tensorflow.python.framework import ops
import nltk
nltk.download('stopwords')
ops.reset_default_graph()

class Triplet_Network_Model(BaseModel):

    def __init__(self, data_loader, config):
        super(Triplet_Network_Model, self).__init__(config)
        # Get the data_loader to make the joint of the inputs in the graph
        self.data_loader = data_loader

        # define some important variables
        self.train_anchor_data = []
        self.train_positive_data = []
        self.train_negative_data = []
        self.y_target_placeholder = []
        self.embedding_placeholder = []
        # self.is_training = None
        self.d_pos = None
        self.d_neg = None
        self.loss = None
        self.accuracy = None
        self.my_opt = None
        self.train_step = None

        self.build_model()
        self.init_saver()

    def build_model(self):
        # here you build the tensorflow graph of any model you want and also define the loss.

        """
        :return:
        """

        """
        Helper Variables
        """
        # self.global_step_tensor = tf.Variable(0, trainable=False, name='global_step')
        # self.global_step_inc = self.global_step_tensor.assign(self.global_step_tensor + 1)
        #self.global_epoch_tensor = tf.Variable(0, trainable=False, name='global_epoch')
        #self.global_epoch_inc = self.global_epoch_tensor.assign(self.global_epoch_tensor + 1)

        """
        Inputs to the network
        """
        with tf.variable_scope('inputs'):
            A = tf.Variable(tf.random_normal(shape=[self.config.NEURONS, self.config.NEURONS]))
            b = tf.Variable(tf.random_normal(shape=[1, self.config.NEURONS]))
            AA = tf.Variable(tf.random_normal(shape=[self.config.NEURONS, self.config.NEURONS]))
            bb = tf.Variable(tf.random_normal(shape=[1, self.config.NEURONS]))

            # Initialize placeholders.
            self.train_anchor_data, self.train_positive_data, self.train_negative_data, \
            self.y_target_placeholder, self.embedding_placeholder, self.learn_rate = self.data_loader.get_input()
            # self.is_training = tf.placeholder(tf.bool, name='Training_flag')
        tf.add_to_collection('inputs', self.train_anchor_data)
        tf.add_to_collection('inputs', self.train_positive_data)
        tf.add_to_collection('inputs', self.train_negative_data)
        tf.add_to_collection('inputs', self.y_target_placeholder)
        tf.add_to_collection('inputs', self.embedding_placeholder)
        tf.add_to_collection('inputs', self.learn_rate)
        # tf.add_to_collection('inputs', self.is_training)

        """
        Network Architecture
        """

        # TODO batch normalization ; dropout

        # RNN for the context.
        with tf.variable_scope('context') as scope:
            result_anchor = Triplet_Network_Model.rnns(self.train_anchor_data, self.embedding_placeholder, self.config.NEURONS)

            # RNN for the response.
        with tf.variable_scope("responses") as scope:
            result_positive = Triplet_Network_Model.rnns(self.train_positive_data,self.embedding_placeholder,
                                                         self.config.NEURONS, hstate=result_anchor)
            scope.reuse_variables()
            result_negative = Triplet_Network_Model.rnns(self.train_negative_data,self.embedding_placeholder,
                                                         self.config.NEURONS, hstate=result_anchor)

        with tf.variable_scope('out'):
            result_anchor_2 = tf.sigmoid(tf.add(tf.matmul(result_anchor, A), b))
            result_positive_2 = tf.sigmoid(tf.add(tf.matmul(result_positive, AA), bb))
            result_negative_2 = tf.sigmoid(tf.add(tf.matmul(result_negative, AA), bb))

            # anc_pos = tf.log(2+result_anchor_2) - tf.log(2+result_positive_2) # tf.nn.softmax(
            # anc_neg = tf.log(2+result_anchor_2) - tf.log(2+result_negative_2) # tf.nn.softmax(

            anc_pos = result_anchor_2 - result_positive_2  # tf.nn.softmax(
            anc_neg = result_anchor_2 - result_negative_2  # tf.nn.softmax(

            with tf.variable_scope('d_pos-d_neg'):
                self.d_pos = tf.reduce_sum(tf.square(anc_pos), 1)  # tf.divide( , NEURONS)
                self.d_neg = tf.reduce_sum(tf.square(anc_neg), 1)

                tf.add_to_collection('d_pos', self.d_pos)
                tf.add_to_collection('d_neg', self.d_neg)

            # tf.summary.histogram("anc_pos", anc_pos)
            # tf.summary.histogram("anc_neg", anc_neg)

        """
        Some operators for the training process
        """

        with tf.variable_scope('loss-acc'):
            # Declare loss function (Cross Entropy loss).
            los_ = tf.maximum(0., self.config.margin + self.d_pos - self.d_neg)  # because d_pos and d_neg are squared distances
            self.loss = tf.reduce_mean(los_)

            # Prediction 0/1
            #prediction = tf.cast(tf.greater(self.d_neg + margin_2, self.d_pos), tf.float32)  # +margin_2
            prediction = tf.cast(tf.greater(self.d_neg + self.config.margin , self.d_pos), tf.float32)  # +margin_2
            predictions_correct = tf.cast(tf.equal(prediction, self.y_target_placeholder), tf.float32)
            self.accuracy = tf.reduce_mean(predictions_correct)

        with tf.variable_scope('train_step'):
            # learn_rate = tf.placeholder(tf.float32, shape=[])
            # Declare optimizer.
            self.my_opt = tf.train.AdamOptimizer(learning_rate=self.learn_rate,
                                                 beta1=0.9,
                                                 beta2=0.999,
                                                 epsilon=1e-08)  # 0.0025 GradientDescentOptimizer
            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies(update_ops):
                self.train_step = self.my_opt.minimize(self.loss)
                # self.train_step = self.my_opt.minimize(self.loss, global_step = self.global_step_tensor)

        tf.add_to_collection('train', self.train_step)
        tf.add_to_collection('train', self.loss)
        tf.add_to_collection('train', self.accuracy)

    def rnns(data,embedding_placeholder,NEURONS, hstate=None):
        #    with tf.variable_scope('anchor'):
        word_vectors = tf.nn.embedding_lookup(embedding_placeholder, data)
        # RETURNS: Tensor of [batch_size, MAX_DOCUMENT_LENGTH, EMBEDDING_SIZE] with embedded sequences.
        word_list = tf.unstack(word_vectors, axis=1)  # [batch_size, EMBEDDING_SIZE]
        cell1 = tf.nn.rnn_cell.GRUCell(NEURONS,
                                       activation=tf.sigmoid,
                                       kernel_initializer=tf.orthogonal_initializer())
        cell1 = tf.contrib.rnn.DropoutWrapper(cell1, input_keep_prob=0.2, output_keep_prob=0.2)
        output, final_state = tf.nn.static_rnn(cell1, word_list,
                                               initial_state=hstate,
                                               dtype=tf.float32)
        return final_state

    def init_saver(self):
        """
        initialize the tensorflow saver that will be used in saving the checkpoints.
        :return:
        """
        # self.saver = tf.train.Saver(max_to_keep=self.config.max_to_keep, save_relative_paths=True)
        self.saver = tf.train.Saver()


