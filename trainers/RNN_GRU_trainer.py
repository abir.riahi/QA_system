from base.base_train import BaseTrain
from tqdm import tqdm
import numpy as np

import tensorflow as tf

class RNN_GRU_Trainer(BaseTrain):

    def __init__(self, sess, model, config, logger, data_loader):
        """
        Constructing the Cifar trainer based on the Base Train..
        Here is the pipeline of constructing
        - Assign sess, model, config, logger, data_loader(if_specified)
        - Initialize all variables
        - Load the latest checkpoint
        - Create the summarizer
        - Get the nodes we will need to run it from the graph
        :param sess:
        :param model:
        :param config:
        :param logger:
        :param data_loader:
        """
        super(RNN_GRU_Trainer, self).__init__(sess, model, config, logger, data_loader)

        # load the model from the latest checkpoint
        # self.model.load(self.sess)

        # Summarizer
        self.summarizer = logger

        self.context_placeholder, self.response_placeholder, \
        self.y_target_placeholder, self.embedding_placeholder, self.learn_rate = tf.get_collection('inputs')
        self.train_step, self.loss, self.accuracy = tf.get_collection('train')

    def train(self, file_res=None):
        """
        This is the main loop of training
        Looping on the epochs
        :return:
        """

        # Intitialize Variables
        init_op = tf.global_variables_initializer()

        # Add ops to save and restore all the variables.
        saver = tf.train.Saver()  # max_to_keep=how_many_models_save

        self.sess.run(init_op)

        train_loss = []
        valid_loss = []
        test_loss = []

        train_acc = []
        valid_acc = []
        test_acc = []

        count = 0

        recall_dict = {}
        recall_dict[1] = []
        recall_dict[2] = []
        recall_dict[3] = []
        recall_dict[5] = []

        for epoch in range(self.config.EPOCHS):

            """
            Train one epoch
            :param epoch: cur epoch number
            :return:
            """

            train_loss_batches = []
            train_acc_batches = []

            # TRAINING PART
            for l in tqdm(np.arange(0, self.data_loader.train_len, self.config.batch_size), desc="Training part"):
                _, train_loss_temp, train_acc_temp = self.sess.run([self.train_step, self.loss, self.accuracy],
                                                              feed_dict={
                                                                  self.context_placeholder: self.data_loader.train_context_ids[l:l + self.config.batch_size],
                                                                  self.response_placeholder: self.data_loader.train_response_ids[
                                                                                        l:l + self.config.batch_size],
                                                                  self.y_target_placeholder: np.transpose(
                                                                      [self.data_loader.train_targets[l:l + self.config.batch_size]]),
                                                                  self.embedding_placeholder: self.data_loader.embedding,
                                                                  self.learn_rate: RNN_GRU_Trainer.step_decay(epoch)})

                train_loss_batches.append(train_loss_temp)
                train_acc_batches.append(train_acc_temp)

            train_loss.append(np.mean(train_loss_batches))
            train_acc.append(np.mean(train_acc_batches))

            # VALIDATION PART
            if (epoch + 1) % 1 == 0:  # 100
                predictions_cor = []
                predictions_distr_0 = []
                predictions_distr_1 = []
                predictions_distr_2 = []
                predictions_distr_3 = []
                predictions_distr_4 = []
                predictions_distr_5 = []
                predictions_distr_6 = []
                predictions_distr_7 = []
                predictions_distr_8 = []

                valid_loss_batches = []
                valid_acc_batches = []

                predictions_cor_batches = []
                predictions_distr_0_batches = []
                predictions_distr_1_batches = []
                predictions_distr_2_batches = []
                predictions_distr_3_batches = []
                predictions_distr_4_batches = []
                predictions_distr_5_batches = []
                predictions_distr_6_batches = []
                predictions_distr_7_batches = []
                predictions_distr_8_batches = []

                for z in tqdm(np.arange(0, self.data_loader.valid_len, self.config.batch_size), desc="Validation part"):
                    valid_loss_temp, valid_acc_temp, predictions_cor_temp = self.sess.run([self.loss, self.accuracy, self.model.prob_prediction],
                                                                                     feed_dict={
                                                                                         self.context_placeholder: self.data_loader.valid_context_ids[
                                                                                                              z:z + self.config.batch_size],
                                                                                         self.response_placeholder: self.data_loader.valid_response_ids[
                                                                                                               z:z + self.config.batch_size],
                                                                                         self.y_target_placeholder: np.transpose(
                                                                                             [self.data_loader.valid_targets[
                                                                                              z:z + self.config.batch_size]]),
                                                                                         self.embedding_placeholder: self.data_loader.embedding})

                    valid_loss_batches.append(valid_loss_temp)
                    valid_acc_batches.append(valid_acc_temp)

                    for s in range(len(predictions_cor_temp)):
                        predictions_cor_batches.append(predictions_cor_temp[s][0])

                        # Calculate the model output for the 1st distractor response
                    predictions_distr_0_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_0_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})

                    for s in range(len(predictions_distr_0_temp)):
                        predictions_distr_0_batches.append(predictions_distr_0_temp[s][0])

                    # Calculate the model output for the 2nd distractor response
                    predictions_distr_1_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_1_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_1_temp)):
                        predictions_distr_1_batches.append(predictions_distr_1_temp[s][0])

                    # Calculate the model output for the 3rd distractor response
                    predictions_distr_2_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_2_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_2_temp)):
                        predictions_distr_2_batches.append(predictions_distr_2_temp[s][0])

                        # Calculate the model output for the 4th distractor response
                    predictions_distr_3_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_3_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_3_temp)):
                        predictions_distr_3_batches.append(predictions_distr_3_temp[s][0])

                        # Calculate the model output for the 5th distractor response
                    predictions_distr_4_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_4_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_4_temp)):
                        predictions_distr_4_batches.append(predictions_distr_4_temp[s][0])

                        # Calculate the model output for the 6th distractor response
                    predictions_distr_5_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_5_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_5_temp)):
                        predictions_distr_5_batches.append(predictions_distr_5_temp[s][0])

                    # Calculate the model output for the 7th distractor response
                    predictions_distr_6_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_6_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_6_temp)):
                        predictions_distr_6_batches.append(predictions_distr_6_temp[s][0])

                        # Calculate the model output for the 8th distractor response
                    predictions_distr_7_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_7_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_7_temp)):
                        predictions_distr_7_batches.append(predictions_distr_7_temp[s][0])

                        # Calculate the model output for the 9th distractor response
                    predictions_distr_8_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_8_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_8_temp)):
                        predictions_distr_8_batches.append(predictions_distr_8_temp[s][0])

                predictions_cor.append(predictions_cor_batches)
                predictions_distr_0.append(predictions_distr_0_batches)
                predictions_distr_1.append(predictions_distr_1_batches)
                predictions_distr_2.append(predictions_distr_2_batches)
                predictions_distr_3.append(predictions_distr_3_batches)
                predictions_distr_4.append(predictions_distr_4_batches)
                predictions_distr_5.append(predictions_distr_5_batches)
                predictions_distr_6.append(predictions_distr_6_batches)
                predictions_distr_7.append(predictions_distr_7_batches)
                predictions_distr_8.append(predictions_distr_8_batches)

                valid_loss.append(np.mean(valid_loss_batches))
                valid_acc.append(np.mean(valid_acc_batches))

                # Printing to see the process and calculate the recall
                acc_and_loss = [epoch + 1, train_loss[count], valid_loss[count], train_acc[epoch], valid_acc[count]]
                acc_and_loss = [np.round(x, 2) for x in acc_and_loss]
                count += 1

                print(
                    'Generation # {}. Train Loss (Valid Loss): {:.2f} ({:.2f}). Train Acc (Valid Acc): {:.2f} ({:.2f})'.format(
                        *acc_and_loss))
                print('The learning rate used at this iterartion was: ' + str(RNN_GRU_Trainer.step_decay(epoch)))
                file_res.write(
                    'Generation # {}. Train Loss (Valid Loss): {:.2f} ({:.2f}). Train Acc (Valid Acc): {:.2f} ({:.2f}) \n'.format(
                        *acc_and_loss))
                file_res.write('The learning rate used at this iteration was: ' + str(RNN_GRU_Trainer.step_decay(epoch)) + '\n')

                total_results = np.column_stack([np.array(predictions_cor).T, np.array(predictions_distr_0).T,
                                                 np.array(predictions_distr_1).T, np.array(predictions_distr_2).T,
                                                 np.array(predictions_distr_3).T, np.array(predictions_distr_4).T,
                                                 np.array(predictions_distr_5).T, np.array(predictions_distr_6).T,
                                                 np.array(predictions_distr_7).T, np.array(predictions_distr_8).T
                                                 ])

                # Order the rows and output indexes, the correct one is index 0.
                # So if in each line index 0 is the first one....BINGO!!!
                reult_order = np.argsort(total_results, axis=1)
                y_valid_recall = np.zeros(self.data_loader.valid_len)  # because the correct one is at position 0

                for n in [1, 2, 3, 5]:
                    recall = RNN_GRU_Trainer.evaluate_recall(reult_order, y_valid_recall, n)
                    recall_dict[n].append(recall)
                    print("Recall @ ({}, 10): {:g}".format(n, recall))
                    file_res.write("Recall @ ({}, 10): {:g} \n".format(n, recall))
                print('Train accuracy ' + str(train_acc))
                file_res.write('Train accuracy ' + str(train_acc) + '\n')
                print('Validation accuracy ' + str(valid_acc))
                file_res.write('Validation accuracy ' + str(valid_acc) + '\n')
                print('Train loss ' + str(train_loss))
                file_res.write('Train loss ' + str(train_loss) + '\n')
                print('Validation loss ' + str(valid_loss))
                file_res.write('Validation loss ' + str(valid_loss) + '\n')
                print('Recall at 1 ' + str(recall_dict[1]))
                file_res.write('Recall at 1 ' + str(recall_dict[1]) + '\n')
                print('Recall at 2 ' + str(recall_dict[2]))
                file_res.write('Recall at 2 ' + str(recall_dict[2]) + '\n')
                print('Recall at 3 ' + str(recall_dict[3]))
                file_res.write('Recall at 3 ' + str(recall_dict[3]) + '\n')
                print('Recall at 5 ' + str(recall_dict[5]))
                file_res.write('Recall at 5 ' + str(recall_dict[5]) + '\n')
                file_res.write('\n')
                file_res.write('\n')

                if epoch > 0 and np.mean(valid_acc_batches) > self.config.max_acc:
                    self.config.max_acc = np.mean(valid_acc_batches)
                    self.config.early_stop_count = self.config.MAX_early_stop_count
                    save_path = saver.save(self.sess, "C:/Users/user/Desktop/internship/data/RNN_GRU_data/Best_Model.ckpt")
                    print("Model saved in path: %s" % save_path)
                else:
                    self.config.early_stop_count -= 1

            # TESTING PART
            if (epoch + 1) % self.config.EPOCHS == 0 or self.config.early_stop_count == 0:  # So at the end test the last model on the testing set.
                if self.config.early_stop_count == 0:
                    saver.restore(self.sess, "C:/Users/user/Desktop/internship/data/RNN_GRU_data/Best_Model.ckpt")

                predictions_cor = []
                predictions_distr_0 = []
                predictions_distr_1 = []
                predictions_distr_2 = []
                predictions_distr_3 = []
                predictions_distr_4 = []
                predictions_distr_5 = []
                predictions_distr_6 = []
                predictions_distr_7 = []
                predictions_distr_8 = []
                test_loss_batches = []
                test_acc_batches = []
                predictions_cor_batches = []
                predictions_distr_0_batches = []
                predictions_distr_1_batches = []
                predictions_distr_2_batches = []
                predictions_distr_3_batches = []
                predictions_distr_4_batches = []
                predictions_distr_5_batches = []
                predictions_distr_6_batches = []
                predictions_distr_7_batches = []
                predictions_distr_8_batches = []
                for z in tqdm(np.arange(0, self.data_loader.test_len, self.config.batch_size), desc="Test part"):
                    test_loss_temp, test_acc_temp, predictions_cor_temp = self.sess.run([self.loss, self.accuracy, self.model.prob_prediction],
                                                                                   feed_dict={
                                                                                       self.context_placeholder: self.data_loader.test_context_ids[
                                                                                                            z:z + self.config.batch_size],
                                                                                       self.response_placeholder: self.data_loader.test_response_ids[
                                                                                                             z:z + self.config.batch_size],
                                                                                       self.y_target_placeholder: np.transpose([
                                                                                           self.data_loader.test_targets[
                                                                                                                              z:z + self.config.batch_size]]),
                                                                                       self.embedding_placeholder: self.data_loader.embedding})
                    test_loss_batches.append(test_loss_temp)
                    test_acc_batches.append(test_acc_temp)
                    for s in range(len(predictions_cor_temp)):
                        predictions_cor_batches.append(predictions_cor_temp[s][0])

                        # Calculate the model output for the 1st distractor response
                    predictions_distr_0_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_0_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_0_temp)):
                        predictions_distr_0_batches.append(predictions_distr_0_temp[s][0])

                    # Calculate the model output for the 2nd distractor response
                    predictions_distr_1_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_1_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_1_temp)):
                        predictions_distr_1_batches.append(predictions_distr_1_temp[s][0])

                    # Calculate the model output for the 3rd distractor response
                    predictions_distr_2_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_2_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_2_temp)):
                        predictions_distr_2_batches.append(predictions_distr_2_temp[s][0])

                        # Calculate the model output for the 4th distractor response
                    predictions_distr_3_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_3_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_3_temp)):
                        predictions_distr_3_batches.append(predictions_distr_3_temp[s][0])

                        # Calculate the model output for the 5th distractor response
                    predictions_distr_4_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_4_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_4_temp)):
                        predictions_distr_4_batches.append(predictions_distr_4_temp[s][0])

                        # Calculate the model output for the 6th distractor response
                    predictions_distr_5_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_5_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_5_temp)):
                        predictions_distr_5_batches.append(predictions_distr_5_temp[s][0])

                        # Calculate the model output for the 7th distractor response
                    predictions_distr_6_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_6_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_6_temp)):
                        predictions_distr_6_batches.append(predictions_distr_6_temp[s][0])

                        # Calculate the model output for the 8th distractor response
                    predictions_distr_7_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_7_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_7_temp)):
                        predictions_distr_7_batches.append(predictions_distr_7_temp[s][0])

                        # Calculate the model output for the 9th distractor response
                    predictions_distr_8_temp = self.sess.run(self.model.prob_prediction, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_8_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_8_temp)):
                        predictions_distr_8_batches.append(predictions_distr_8_temp[s][0])

                predictions_cor.append(predictions_cor_batches)
                predictions_distr_0.append(predictions_distr_0_batches)
                predictions_distr_1.append(predictions_distr_1_batches)
                predictions_distr_2.append(predictions_distr_2_batches)
                predictions_distr_3.append(predictions_distr_3_batches)
                predictions_distr_4.append(predictions_distr_4_batches)
                predictions_distr_5.append(predictions_distr_5_batches)
                predictions_distr_6.append(predictions_distr_6_batches)
                predictions_distr_7.append(predictions_distr_7_batches)
                predictions_distr_8.append(predictions_distr_8_batches)

                test_loss = np.mean(test_loss_batches)
                test_acc = np.mean(test_acc_batches)

                print('The results on the test set are: ')
                file_res.write('The results on the test set are: \n')
                print('The test loss is: ' + str(test_loss))
                file_res.write('The test loss is: ' + str(test_loss) + '\n')
                print('The test accuracy is: ' + str(test_acc))
                file_res.write('The test accuracy is: ' + str(test_acc) + '\n')

                total_results = np.column_stack([np.array(predictions_cor).T, np.array(predictions_distr_0).T,
                                                 np.array(predictions_distr_1).T, np.array(predictions_distr_2).T,
                                                 np.array(predictions_distr_3).T, np.array(predictions_distr_4).T,
                                                 np.array(predictions_distr_5).T, np.array(predictions_distr_6).T,
                                                 np.array(predictions_distr_7).T, np.array(predictions_distr_8).T
                                                 ])
                # Order the rows and output indexes, the correct one is index 0.
                # So if in each line index 0 is the first one....BINGO!!!
                reult_order = np.argsort(total_results, axis=1)
                y_test_recall = np.zeros(self.data_loader.test_len)  # because the correct one is at position 0

                for n in [1, 2, 3, 5]:
                    recall = RNN_GRU_Trainer.evaluate_recall(reult_order, y_test_recall, n)
                    print("Recall @ ({}, 10): {:g}".format(n, recall))
                    file_res.write("Recall @ ({}, 10): {:g} \n".format(n, recall))
                break

            # summarize
                #summaries_dict = {'train/loss_per_epoch': train_loss,
                #             'train/acc_per_epoch': train_acc,
                #             'valid/loss_per_epoch': valid_loss,
                #             'valid/acc_per_epoch': valid_acc,
                #             'test/loss_per_epoch': test_loss,
                #             'test/acc_per_epoch': test_acc}

            #self.summarizer.summarize(self.model.global_step_tensor.eval(self.sess), summaries_dict)

            #self.model.save(self.sess)


    @staticmethod
    def step_decay(epoch, initial_lrate=0.0025, drop=0.5, epochs_drop = 5):
        # Function to define the learning rate for the optimizer.
        return initial_lrate * (drop**((1+epoch)//epochs_drop))

    @staticmethod
    def evaluate_recall(y, y_test, k=1,  name=None):
        # Define function to calculate the recall.
        with tf.name_scope(name, default_name="evaluate_recall"):
            num_examples = float(len(y))
            num_correct = 0
            for predictions, label in zip(y, y_test):
                # PUT -K because the one with higher prob is the last one.
                if label in predictions[-k:]:
                    num_correct += 1
            return num_correct / num_examples