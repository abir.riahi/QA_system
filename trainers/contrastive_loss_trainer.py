from base.base_train import BaseTrain
from tqdm import tqdm
import numpy as np
import pandas as pd

import tensorflow as tf

class Contrastive_Loss_Trainer(BaseTrain):

    def __init__(self, sess, model, config, logger, data_loader):
        """
        Constructing the Cifar trainer based on the Base Train..
        Here is the pipeline of constructing
        - Assign sess, model, config, logger, data_loader(if_specified)
        - Initialize all variables
        - Load the latest checkpoint
        - Create the summarizer
        - Get the nodes we will need to run it from the graph
        :param sess:
        :param model:
        :param config:
        :param logger:
        :param data_loader:
        """
        super(Contrastive_Loss_Trainer, self).__init__(sess, model, config, logger, data_loader)

        # load the model from the latest checkpoint
        #self.model.load(self.sess)

        # Summarizer
        self.summarizer = logger

        self.context_placeholder, self.response_placeholder, \
        self.y_target_placeholder, self.embedding_placeholder, self.learn_rate = tf.get_collection('inputs')
        self.train_step, self.loss, self.accuracy = tf.get_collection('train')

    def train(self, file_res=None):
        """
        This is the main loop of training
        Looping on the epochs
        :return:
        """

        # Intitialize Variables
        init_op = tf.global_variables_initializer()

        # Add ops to save and restore all the variables.
        saver = tf.train.Saver()  # max_to_keep=how_many_models_save

        self.sess.run(init_op)

        train_loss = []
        valid_loss = []

        train_acc = []
        valid_acc = []

        count = 0

        recall_dict = {}
        recall_dict[1] = []
        recall_dict[2] = []
        recall_dict[3] = []
        recall_dict[5] = []

        for epoch in range(self.config.EPOCHS):

            """
            Train one epoch
            :param epoch: cur epoch number
            :return:
            """

            # TRAINING PART

            train_loss_batches = []
            train_acc_batches = []

            for l in tqdm(np.arange(0, self.data_loader.train_len, self.config.batch_size), desc="Training part"):
                _, train_loss_temp, train_acc_temp = self.sess.run([self.train_step, self.loss, self.accuracy],
                                                              feed_dict={
                                                                  self.context_placeholder: self.data_loader.train_context_ids[l:l + self.config.batch_size],
                                                                  self.response_placeholder: self.data_loader.train_response_ids[
                                                                                        l:l + self.config.batch_size],
                                                                  self.y_target_placeholder: np.transpose(
                                                                      [self.data_loader.train_targets[l:l + self.config.batch_size]]),
                                                                  self.embedding_placeholder: self.data_loader.embedding,
                                                                  self.learn_rate: Contrastive_Loss_Trainer.step_decay(epoch)})

                train_loss_batches.append(train_loss_temp)
                train_acc_batches.append(train_acc_temp)

            train_loss.append(np.mean(train_loss_batches))
            train_acc.append(np.mean(train_acc_batches))

            # VALIDATION PART
            if (epoch + 1) % 1 == 0:  # 100
                predictions_cor = []
                predictions_distr_0 = []
                predictions_distr_1 = []
                predictions_distr_2 = []
                predictions_distr_3 = []
                predictions_distr_4 = []
                predictions_distr_5 = []
                predictions_distr_6 = []
                predictions_distr_7 = []
                predictions_distr_8 = []

                valid_loss_batches = []
                valid_acc_batches = []

                predictions_cor_batches = []
                predictions_distr_0_batches = []
                predictions_distr_1_batches = []
                predictions_distr_2_batches = []
                predictions_distr_3_batches = []
                predictions_distr_4_batches = []
                predictions_distr_5_batches = []
                predictions_distr_6_batches = []
                predictions_distr_7_batches = []
                predictions_distr_8_batches = []

                for z in tqdm(np.arange(0, self.data_loader.valid_len, self.config.batch_size), desc="Validation part"):
                    valid_loss_temp, valid_acc_temp = self.sess.run([self.loss, self.accuracy],
                                                                                     feed_dict={
                                                                                         self.context_placeholder: self.data_loader.valid_context_ids[
                                                                                                              z:z + self.config.batch_size],
                                                                                         self.response_placeholder: self.data_loader.valid_50_50[
                                                                                                               z:z + self.config.batch_size],
                                                                                         self.y_target_placeholder: np.transpose(
                                                                                             [self.data_loader.valid_50_50_labels[
                                                                                              z:z + self.config.batch_size]]),
                                                                                         self.embedding_placeholder: self.data_loader.embedding})

                    valid_loss_batches.append(valid_loss_temp)
                    valid_acc_batches.append(valid_acc_temp)

                    predictions_cor_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_response_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})

                    for s in range(len(predictions_cor_temp)):
                        predictions_cor_batches.append(predictions_cor_temp[s][0])

                        # Calculate the model output for the 1st distractor response
                    predictions_distr_0_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_0_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})

                    for s in range(len(predictions_distr_0_temp)):
                        predictions_distr_0_batches.append(predictions_distr_0_temp[s][0])

                    # Calculate the model output for the 2nd distractor response
                    predictions_distr_1_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_1_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_1_temp)):
                        predictions_distr_1_batches.append(predictions_distr_1_temp[s][0])

                    # Calculate the model output for the 3rd distractor response
                    predictions_distr_2_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_2_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_2_temp)):
                        predictions_distr_2_batches.append(predictions_distr_2_temp[s][0])

                        # Calculate the model output for the 4th distractor response
                    predictions_distr_3_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_3_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_3_temp)):
                        predictions_distr_3_batches.append(predictions_distr_3_temp[s][0])

                        # Calculate the model output for the 5th distractor response
                    predictions_distr_4_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_4_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_4_temp)):
                        predictions_distr_4_batches.append(predictions_distr_4_temp[s][0])

                        # Calculate the model output for the 6th distractor response
                    predictions_distr_5_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_5_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_5_temp)):
                        predictions_distr_5_batches.append(predictions_distr_5_temp[s][0])

                    # Calculate the model output for the 7th distractor response
                    predictions_distr_6_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_6_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_6_temp)):
                        predictions_distr_6_batches.append(predictions_distr_6_temp[s][0])

                        # Calculate the model output for the 8th distractor response
                    predictions_distr_7_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_7_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_7_temp)):
                        predictions_distr_7_batches.append(predictions_distr_7_temp[s][0])

                        # Calculate the model output for the 9th distractor response
                    predictions_distr_8_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.valid_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.valid_Distractor_8_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_8_temp)):
                        predictions_distr_8_batches.append(predictions_distr_8_temp[s][0])

                predictions_cor.append(predictions_cor_batches)
                predictions_distr_0.append(predictions_distr_0_batches)
                predictions_distr_1.append(predictions_distr_1_batches)
                predictions_distr_2.append(predictions_distr_2_batches)
                predictions_distr_3.append(predictions_distr_3_batches)
                predictions_distr_4.append(predictions_distr_4_batches)
                predictions_distr_5.append(predictions_distr_5_batches)
                predictions_distr_6.append(predictions_distr_6_batches)
                predictions_distr_7.append(predictions_distr_7_batches)
                predictions_distr_8.append(predictions_distr_8_batches)

                valid_loss.append(np.mean(valid_loss_batches))
                valid_acc.append(np.mean(valid_acc_batches))

                # Printing to see the process and calculate the recall
                acc_and_loss = [epoch + 1, train_loss[count], valid_loss[count], train_acc[epoch], valid_acc[count]]
                acc_and_loss = [np.round(x, 2) for x in acc_and_loss]
                count += 1

                print(
                    'Generation # {}. Train Loss (Valid Loss): {:.2f} ({:.2f}). Train Acc (Valid Acc): {:.2f} ({:.2f})'.format(
                        *acc_and_loss))
                print('The learning rate used at this iterartion was: ' + str(Contrastive_Loss_Trainer.step_decay(epoch)))
                file_res.write(
                    'Generation # {}. Train Loss (Valid Loss): {:.2f} ({:.2f}). Train Acc (Valid Acc): {:.2f} ({:.2f}) \n'.format(
                        *acc_and_loss))
                file_res.write('The learning rate used at this iteration was: ' + str(Contrastive_Loss_Trainer.step_decay(epoch)) + '\n')

                total_results = np.column_stack([np.array(predictions_cor).T, np.array(predictions_distr_0).T,
                                                 np.array(predictions_distr_1).T, np.array(predictions_distr_2).T,
                                                 np.array(predictions_distr_3).T, np.array(predictions_distr_4).T,
                                                 np.array(predictions_distr_5).T, np.array(predictions_distr_6).T,
                                                 np.array(predictions_distr_7).T, np.array(predictions_distr_8).T
                                                 ])

                # Order the rows and output indexes, the correct one is index 0.
                # So if in each line index 0 is the first one....BINGO!!!
                reult_order = np.argsort(total_results, axis=1)
                y_valid_recall = np.zeros(self.data_loader.valid_len)  # because the correct one is at position 0

                for n in [1, 2, 3, 5]:
                    recall = Contrastive_Loss_Trainer.evaluate_recall(reult_order, y_valid_recall, n)
                    recall_dict[n].append(recall)
                    print("Recall @ ({}, 10): {:g}".format(n, recall))
                    file_res.write("Recall @ ({}, 10): {:g} \n".format(n, recall))
                print('Train accuracy ' + str(train_acc))
                file_res.write('Train accuracy ' + str(train_acc) + '\n')
                print('Validation accuracy ' + str(valid_acc))
                file_res.write('Validation accuracy ' + str(valid_acc) + '\n')
                print('Train loss ' + str(train_loss))
                file_res.write('Train loss ' + str(train_loss) + '\n')
                print('Validation loss ' + str(valid_loss))
                file_res.write('Validation loss ' + str(valid_loss) + '\n')
                print('Recall at 1 ' + str(recall_dict[1]))
                file_res.write('Recall at 1 ' + str(recall_dict[1]) + '\n')
                print('Recall at 2 ' + str(recall_dict[2]))
                file_res.write('Recall at 2 ' + str(recall_dict[2]) + '\n')
                print('Recall at 3 ' + str(recall_dict[3]))
                file_res.write('Recall at 3 ' + str(recall_dict[3]) + '\n')
                print('Recall at 5 ' + str(recall_dict[5]))
                file_res.write('Recall at 5 ' + str(recall_dict[5]) + '\n')
                file_res.write('\n')
                file_res.write('\n')

                if epoch > 0 and np.mean(valid_acc_batches) < self.config.max_loss: # np.mean(valid_acc_batches) > max_acc
                    self.config.max_loss = np.mean(valid_acc_batches)
                    self.config.early_stop_count = self.config.MAX_early_stop_count
                    save_path = saver.save(self.sess, "C:/Users/user/Desktop/internship/data/contrastive_loss_data/Best_Model.ckpt")
                    print("Model saved in path: %s" % save_path)
                else:
                    self.config.early_stop_count -= 1

            print("before test")

            # TESTING PART
            if (epoch + 1) % self.config.EPOCHS == 0 or self.config.early_stop_count == 0:  # So at the end test the last model on the testing set.
                if self.config.early_stop_count == 0:
                    saver.restore(self.sess, "C:/Users/user/Desktop/internship/data/contrastive_loss_data/Best_Model.ckpt")

                print("testing prt")

                predictions_cor = []
                predictions_distr_0 = []
                predictions_distr_1 = []
                predictions_distr_2 = []
                predictions_distr_3 = []
                predictions_distr_4 = []
                predictions_distr_5 = []
                predictions_distr_6 = []
                predictions_distr_7 = []
                predictions_distr_8 = []

                test_loss_batches = []
                test_acc_batches = []

                predictions_cor_batches = []
                predictions_distr_0_batches = []
                predictions_distr_1_batches = []
                predictions_distr_2_batches = []
                predictions_distr_3_batches = []
                predictions_distr_4_batches = []
                predictions_distr_5_batches = []
                predictions_distr_6_batches = []
                predictions_distr_7_batches = []
                predictions_distr_8_batches = []

                for z in tqdm(np.arange(0, self.data_loader.test_len, self.config.batch_size), desc="Test part"):
                    test_loss_temp, test_acc_temp = self.sess.run([self.loss, self.accuracy],
                                                                                   feed_dict={
                                                                                       self.context_placeholder: self.data_loader.test_context_ids[
                                                                                                            z:z + self.config.batch_size],
                                                                                       self.response_placeholder: self.data_loader.test_50_50[
                                                                                                             z:z + self.config.batch_size],
                                                                                       self.y_target_placeholder: np.transpose([
                                                                                           self.data_loader.test_50_50_labels[
                                                                                           z:z + self.config.batch_size]]),
                                                                                       self.embedding_placeholder: self.data_loader.embedding})
                    test_loss_batches.append(test_loss_temp)
                    test_acc_batches.append(test_acc_temp)
                    predictions_cor_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_response_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_cor_temp)):
                        predictions_cor_batches.append(predictions_cor_temp[s][0])

                        # Calculate the model output for the 1st distractor response
                    predictions_distr_0_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_0_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_0_temp)):
                        predictions_distr_0_batches.append(predictions_distr_0_temp[s][0])

                    # Calculate the model output for the 2nd distractor response
                    predictions_distr_1_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_1_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_1_temp)):
                        predictions_distr_1_batches.append(predictions_distr_1_temp[s][0])

                    # Calculate the model output for the 3rd distractor response
                    predictions_distr_2_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_2_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_2_temp)):
                        predictions_distr_2_batches.append(predictions_distr_2_temp[s][0])

                        # Calculate the model output for the 4th distractor response
                    predictions_distr_3_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_3_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_3_temp)):
                        predictions_distr_3_batches.append(predictions_distr_3_temp[s][0])

                        # Calculate the model output for the 5th distractor response
                    predictions_distr_4_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_4_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_4_temp)):
                        predictions_distr_4_batches.append(predictions_distr_4_temp[s][0])

                        # Calculate the model output for the 6th distractor response
                    predictions_distr_5_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_5_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_5_temp)):
                        predictions_distr_5_batches.append(predictions_distr_5_temp[s][0])

                        # Calculate the model output for the 7th distractor response
                    predictions_distr_6_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_6_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_6_temp)):
                        predictions_distr_6_batches.append(predictions_distr_6_temp[s][0])

                        # Calculate the model output for the 8th distractor response
                    predictions_distr_7_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_7_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_7_temp)):
                        predictions_distr_7_batches.append(predictions_distr_7_temp[s][0])

                        # Calculate the model output for the 9th distractor response
                    predictions_distr_8_temp = self.sess.run(self.model.d_sqrt, feed_dict={
                        self.context_placeholder: self.data_loader.test_context_ids[z:z + self.config.batch_size],
                        self.response_placeholder: self.data_loader.test_Distractor_8_ids[z:z + self.config.batch_size],
                        self.embedding_placeholder: self.data_loader.embedding})
                    for s in range(len(predictions_distr_8_temp)):
                        predictions_distr_8_batches.append(predictions_distr_8_temp[s][0])

                predictions_cor.append(predictions_cor_batches)
                predictions_distr_0.append(predictions_distr_0_batches)
                predictions_distr_1.append(predictions_distr_1_batches)
                predictions_distr_2.append(predictions_distr_2_batches)
                predictions_distr_3.append(predictions_distr_3_batches)
                predictions_distr_4.append(predictions_distr_4_batches)
                predictions_distr_5.append(predictions_distr_5_batches)
                predictions_distr_6.append(predictions_distr_6_batches)
                predictions_distr_7.append(predictions_distr_7_batches)
                predictions_distr_8.append(predictions_distr_8_batches)

                test_loss = np.mean(test_loss_batches)
                test_acc = np.mean(test_acc_batches)

                print('The results on the test set are: ')
                file_res.write('The results on the test set are: \n')
                print('The test loss is: ' + str(test_loss))
                file_res.write('The test loss is: ' + str(test_loss) + '\n')
                print('The test accuracy is: ' + str(test_acc))
                file_res.write('The test accuracy is: ' + str(test_acc) + '\n')

                total_results = np.column_stack([np.array(predictions_cor).T, np.array(predictions_distr_0).T,
                                                 np.array(predictions_distr_1).T, np.array(predictions_distr_2).T,
                                                 np.array(predictions_distr_3).T, np.array(predictions_distr_4).T,
                                                 np.array(predictions_distr_5).T, np.array(predictions_distr_6).T,
                                                 np.array(predictions_distr_7).T, np.array(predictions_distr_8).T
                                                 ])
                # Order the rows and output indexes, the correct one is index 0.
                # So if in each line index 0 is the first one....BINGO!!!
                reult_order = np.argsort(total_results, axis=1)
                y_test_recall = np.zeros(self.data_loader.test_len)  # because the correct one is at position 0
                pd.DataFrame(total_results).to_csv('total_results_TEST.csv', sep='\t')
                pd.DataFrame(reult_order).to_csv('reult_order_TEST.csv', sep='\t')

                for n in [1, 2, 3, 5]:
                    recall = Contrastive_Loss_Trainer.evaluate_recall(reult_order, y_test_recall, n)
                    print("Recall @ ({}, 10): {:g}".format(n, recall))
                    file_res.write("Recall @ ({}, 10): {:g} \n".format(n, recall))
                #break

            #self.sess.run(self.model.global_epoch_inc)


            # summarize
            #summaries_dict = {'train/loss_per_epoch': train_loss,
                    #                 'train/acc_per_epoch': train_acc,
                    #          'valid/loss_per_epoch': valid_loss,
                    #          'valid/acc_per_epoch': valid_acc,
                    #          'test/loss_per_epoch': test_loss,
            #          'test/acc_per_epoch': test_acc}

            #self.summarizer.summarize(self.model.global_step_tensor.eval(self.sess), summaries_dict)

            #self.model.save(self.sess)



    @staticmethod
    def step_decay(epoch, initial_lrate=0.001, drop=0.5, epochs_drop = 25):
        # Function to define the learning rate for the optimizer.
        return initial_lrate * (drop**((1+epoch)//epochs_drop))

    @staticmethod
    def evaluate_recall(y, y_test, k=1,  name=None):
        # Define function to calculate the recall.
        with tf.name_scope(name, default_name="evaluate_recall"):
            num_examples = float(len(y))
            num_correct = 0
            for predictions, label in zip(y, y_test):
                # PUT -K because the one with higher prob is the last one.
                if label in predictions[-k:]:
                    num_correct += 1
            return num_correct / num_examples




"""
# Plot train and validation loss over epochs
train_loss = [0.057342436, 0.0486012, 0.045249064, 0.043116532, 0.04061667, 0.038544107, 0.03684803, 0.035771497, 0.035002418, 0.034393832, 0.033858243, 0.0333773, 0.032989807, 0.03260391, 0.032225735, 0.03192305, 0.031648144, 0.03137401, 0.031096635, 0.030859178, 0.030596182]
valid_loss = [0.05125907, 0.047489483, 0.046330348, 0.048166405, 0.05038852, 0.05307637, 0.05488733, 0.055212315, 0.0562454, 0.056824837, 0.057995677, 0.058000498, 0.05954798, 0.05944211, 0.0595579, 0.05879216, 0.0596083, 0.05941717, 0.059694257, 0.05977661, 0.06009151]
i_data = range(len(train_loss))
plt.plot(i_data, train_loss, 'k-', label='Train Loss')
plt.plot(i_data, valid_loss, 'r--', label='Validation Loss', linewidth=4)
plt.title('Cross Entropy Loss per Epoch')
plt.xlabel('Epoch')
plt.ylabel('Cross Entropy Loss')
plt.legend(loc='upper right')
plt.show()

# Plot train and validation accuracy over epochs
train_acc =  [0.4996915, 0.49969673, 0.49970034, 0.49968907, 0.49970445, 0.499684, 0.4997032, 0.4996783, 0.4996967, 0.49969494, 0.4996845, 0.499699, 0.4997072, 0.4996918, 0.4996798, 0.49969319, 0.49968424, 0.49968088, 0.49969146, 0.4996843, 0.49968836, 0.49967927, 0.49969548, 0.4996835, 0.4996839, 0.4996868, 0.4996832, 0.49969044, 0.49969396, 0.49969074, 0.49969685, 0.4996803, 0.4996967, 0.49969506]
valid_acc = [0.5000212, 0.5000354, 0.5000117, 0.5000068, 0.4999662, 0.50001025, 0.50004303, 0.5000109, 0.49998462, 0.49999407, 0.4999626, 0.5000026, 0.49997392, 0.5000384, 0.49997118, 0.5000045, 0.5000302, 0.5000087, 0.5000414, 0.5000285, 0.5000371, 0.50000775, 0.49996322, 0.500012, 0.49997997, 0.49998993, 0.49996725, 0.49997118, 0.5000094, 0.49997446, 0.5000038, 0.49997097, 0.49999204, 0.49996915]
i_data = range(len(train_acc))
plt.plot(i_data, train_acc, 'k-', label='Train Set Accuracy')
plt.plot(i_data, valid_acc, 'r--', label='Validation Set Accuracy', linewidth=4)
plt.title('Train and Validation Accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.legend(loc='lower right')
plt.gca().yaxis.grid()
plt.show()

# Plot Recall of validation set over epochs
rec_at_1 = [0.12093098958333333, 0.12928602430555555, 0.1179470486111111, 0.12272135416666667, 0.1247829861111111, 0.12022569444444445, 0.12017144097222222, 0.12310112847222222, 0.1232638888888889, 0.12169053819444445, 0.11865234375, 0.11979166666666667, 0.11707899305555555, 0.11583116319444445, 0.11870659722222222, 0.12141927083333333, 0.11659071180555555, 0.1150173611111111, 0.1193576388888889, 0.11637369791666667, 0.1247287326388889, 0.1184353298611111, 0.11675347222222222, 0.11138237847222222, 0.1139865451388889, 0.1174045138888889, 0.11756727430555555, 0.11707899305555555, 0.11338975694444445, 0.11555989583333333, 0.1188693576388889, 0.11317274305555555, 0.11436631944444445, 0.11881510416666667, 0.1193576388888889, 0.11854383680555555, 0.11344401041666667, 0.11458333333333333, 0.1149631076388889, 0.1181640625, 0.11821831597222222, 0.11409505208333333, 0.11442057291666667, 0.11962890625, 0.11322699652777778, 0.1144748263888889, 0.11767578125, 0.11338975694444445, 0.11371527777777778, 0.11046006944444445]
rec_at_2 = [0.2444118923611111, 0.2695855034722222, 0.2426215277777778, 0.2531467013888889, 0.2599283854166667, 0.24869791666666666, 0.2533094618055556, 0.2616102430555556, 0.25927734375, 0.2601996527777778, 0.2561848958333333, 0.2532552083333333, 0.2501085069444444, 0.2426215277777778, 0.2513020833333333, 0.2514105902777778, 0.24983723958333334, 0.23909505208333334, 0.2547200520833333, 0.24745008680555555, 0.2649739583333333, 0.24739583333333334, 0.2471245659722222, 0.23470052083333334, 0.23942057291666666, 0.2560763888888889, 0.2487521701388889, 0.2439236111111111, 0.24202473958333334, 0.24479166666666666, 0.2530381944444444, 0.24403211805555555, 0.24202473958333334, 0.2578667534722222, 0.2562934027777778, 0.2571614583333333, 0.2437065972222222, 0.2506510416666667, 0.2439236111111111, 0.24989149305555555, 0.24473741319444445, 0.24403211805555555, 0.2412651909722222, 0.2569444444444444, 0.2401801215277778, 0.2400173611111111, 0.24739583333333334, 0.2390407986111111, 0.2404513888888889, 0.2345920138888889]
rec_at_3 = [0.3697374131944444, 0.40234375, 0.3640407986111111, 0.3870985243055556, 0.3919813368055556, 0.3779296875, 0.3834635416666667, 0.3972439236111111, 0.3861762152777778, 0.3939344618055556, 0.3865017361111111, 0.3840603298611111, 0.3829210069444444, 0.3700086805555556, 0.3802083333333333, 0.3798285590277778, 0.3824869791666667, 0.3683268229166667, 0.3935004340277778, 0.3752712673611111, 0.3926866319444444, 0.3766276041666667, 0.3777126736111111, 0.3586697048611111, 0.3708224826388889, 0.3984917534722222, 0.3805338541666667, 0.3722330729166667, 0.373046875, 0.3712565104166667, 0.388671875, 0.3784722222222222, 0.3687608506944444, 0.3913845486111111, 0.3869900173611111, 0.3924153645833333, 0.3698459201388889, 0.3828125, 0.3739691840277778, 0.3761393229166667, 0.3780381944444444, 0.373046875, 0.369140625, 0.3916015625, 0.3709852430555556, 0.3701714409722222, 0.3766276041666667, 0.3671875, 0.369140625, 0.3640950520833333]
rec_at_5 = [0.5917426215277778, 0.6102973090277778, 0.5900607638888888, 0.6061197916666666, 0.6134982638888888, 0.59033203125, 0.6050347222222222, 0.6212022569444444, 0.6123589409722222, 0.6159396701388888, 0.61279296875, 0.6119791666666666, 0.6094835069444444, 0.6031358506944444, 0.6074761284722222, 0.6023220486111112, 0.6139865451388888, 0.6022677951388888, 0.6197916666666666, 0.5995551215277778, 0.6112738715277778, 0.6096462673611112, 0.6106228298611112, 0.5966796875, 0.6089952256944444, 0.6277126736111112, 0.6177300347222222, 0.6031358506944444, 0.6115993923611112, 0.6006944444444444, 0.6192491319444444, 0.6154513888888888, 0.5977105034722222, 0.6209309895833334, 0.6213650173611112, 0.6202256944444444, 0.6044921875, 0.6222330729166666, 0.6071506076388888, 0.6169162326388888, 0.6197374131944444, 0.60400390625, 0.6006401909722222, 0.6268988715277778, 0.6131727430555556, 0.6031901041666666, 0.6153428819444444, 0.6072048611111112, 0.6130642361111112, 0.6014539930555556]
i_data = range(len(rec_at_1))
plt.plot(i_data, rec_at_1, 'k-', label='rec_at_1', linewidth=4)
plt.plot(i_data, rec_at_2, 'r-', label='rec_at_2', linewidth=4)
plt.plot(i_data, rec_at_3, 'b-', label='rec_at_3', linewidth=4)
plt.plot(i_data, rec_at_5, 'y-', label='rec_at_5', linewidth=4)
plt.title('Validation Recall')
plt.xlabel('Epoch')
plt.ylabel('Recall')
plt.legend(loc='lower right')
plt.gca().yaxis.grid()
plt.show()


# Plot Recall of validation set over epochs
plt.plot(i_data, recall_dict[1], 'k-', label='rec_at_1', linewidth=4)
plt.plot(i_data, recall_dict[2], 'r-', label='rec_at_2', linewidth=4)
plt.plot(i_data, recall_dict[3], 'b-', label='rec_at_3', linewidth=4)
plt.plot(i_data, recall_dict[5], 'y-', label='rec_at_5', linewidth=4)
plt.title('Validation Recall')
plt.xlabel('Epoch')
plt.ylabel('Recall')
plt.legend(loc='upper right')
plt.gca().yaxis.grid()
plt.show()
"""