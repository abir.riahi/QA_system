import tensorflow as tf
import numpy as np
import pandas as pd
from nltk.stem.snowball import SnowballStemmer
import nltk
nltk.download('stopwords')

class GloVe:

    filename = 'glove.6B.300d.txt'

    def __init__(self,config):
        self.config = config

        self.vocab = []
        # Had first row of embd all zeros, it will be the embedding for words
        # not found that will recieve id zero from the processor following.
        self.embd = [[np.zeros(300)]]
        self.vocab_size = len(self.vocab)
        self.embedding_dim = len(self.embd[0])
        # embedding = np.array(embd)
        self.embedding = np.vstack(self.embd)

        self.vocab_dict = None
        self.vocab_dict_inv = None

    #loadGloVe
        file = open(GloVe.filename, encoding="utf8")
        for line in file.readlines():
            row = line.strip().split(' ')
            self.vocab.append(row[0])
            self.embd.append([float(i) for i in row[1:]])
        print('Loaded GloVe!')
        file.close()

    #stemm
        # Stemm the vocab of GloVe.
        self.stemmer = SnowballStemmer("english", ignore_stopwords=True)
        self.vocab = [self.stemmer.stem(word) for word in self.vocab]
        # print(stemmer.stem("maybe"))

    #recreate
        # Recreate the vocab and embedding lists after stemming, since they reduce their size.
        self.new_data = pd.DataFrame({'vocab': self.vocab, 'emd': list(self.embedding[1:])})
        self.new_data = self.new_data.drop_duplicates('vocab')
        self.vocab = list(self.new_data.loc[:, 'vocab'])
        self.embedding = list(self.new_data.loc[:, 'emd'])
        self.embedding.insert(0, np.zeros(300))

    #add_popular
    # Add some words not present in GloVe but very popular in the train data.
    # Will give them a random embedding from Uniform(-0.25,0.25).
    # The most common words in the train set are:
    # print(sorted_counts[:10])
        self.vocab.append('__eou__')
        self.embedding = np.vstack((self.embedding, np.random.uniform(-0.25, 0.25, size=300)))
        self.vocab.append('__eot__')
        self.embedding = np.vstack((self.embedding, np.random.uniform(-0.25, 0.25, size=300)))

    #processor
        self.vocab_processor = tf.contrib.learn.preprocessing.VocabularyProcessor(self.config.MAX_DOCUMENT_LENGTH,
                                                                             tokenizer_fn=GloVe.tokenizer_fn())
        # Fit the vocab from GloVe.
        self.pretrain = self.vocab_processor.fit(self.vocab)

        # Create dictionary of words:id.
        self.vocab_dict = self.vocab_processor.vocabulary_._mapping

        # Create dictionary of id:words, the inverse of the previous one.
        self.vocab_dict_inv = {v: k for k, v in self.vocab_dict.items()}

    #words_number
        # Print total number of words.
        self.n_words = len(self.vocab_processor.vocabulary_)
        print('Total words: %d' % self.n_words)
        #   file_res.write('Total words: %d \n' % self.n_words)


    @staticmethod
    # Init vocab processor.
    def tokenizer_fn(self,iterator):
        return (x.split(" ")[-self.config.MAX_DOCUMENT_LENGTH:] for x in iterator)